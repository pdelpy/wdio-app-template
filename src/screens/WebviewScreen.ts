import WebView from '../helpers/WebView';

const SELECTORS = {
    WEB_VIEW_SCREEN: browser.isAndroid
        ? '*//android.webkit.WebView'
        : '*//XCUIElementTypeWebView',
};

class WebViewScreen extends WebView {
    /**
     * Wait for the screen to be displayed based on Xpath
     *
     * @param {boolean} isShown
     */
    async waitForWebViewIsDisplayedByXpath(isShown = true) {
        return await (await $(SELECTORS.WEB_VIEW_SCREEN)).waitForDisplayed({ reverse: !isShown });
    }
}

export default new WebViewScreen();
