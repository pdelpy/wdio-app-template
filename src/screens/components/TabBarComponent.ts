export default class TabBar {
  public static async openHome(): Promise<void> {
    await (await $("~Home")).click();
  }

  public static async openWebView(): Promise<void> {
    await (await $("~WebView")).click();
  }

  public static async openLogin(): Promise<void> {
    await (await $("~Login")).click();
  }

  public static async openForms(): Promise<void> {
    await (await $("~Forms")).click();
  }

  public static async openSwipe(): Promise<void> {
    await (await $("~Swipe")).click();
  }

  public static async waitForTabBarShown(isShown = true): Promise<void> {
    await (await $("~Home")).waitForDisplayed({ reverse: !isShown });
  }
}
