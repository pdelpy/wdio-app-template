export default class AppScreen {
  public selector;
  constructor(selector: string) {
    this.selector = selector;
  }

  /**
   * Wait for the login screen to be visible
   *
   * @param {boolean} isShown
   * @return {boolean}
   */
  public async waitForIsShown(isShown = true): Promise<true | void> {
    return await (await $(this.selector)).waitForDisplayed({ reverse: !isShown });
  }
}
