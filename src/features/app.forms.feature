@smoke
Feature: WebdriverIO and Appium, interacting with form elements

  Background:
    Given i am on the form screen

  @android @ios
  Scenario: Should be able type in the input and validate the text
    When i type the text "Hello, this is a demo app"
    Then the text result has the value "Hello, this is a demo app"