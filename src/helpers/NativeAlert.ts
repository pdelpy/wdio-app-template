const SELECTORS = {
  ANDROID: {
    ALERT_TITLE:
      '*//android.widget.TextView[@resource-id="android:id/alertTitle"]',
    ALERT_MESSAGE:
      '*//android.widget.TextView[@resource-id="android:id/message"]',
    ALERT_BUTTON: '*//android.widget.Button[@text="{BUTTON_TEXT}"]'
  },
  IOS: {
    ALERT: "-ios predicate string:type == 'XCUIElementTypeAlert'"
  }
};

class NativeAlert {
  /**
   * Wait for the alert to exist
   */
  public static waitForIsShown(isShown = true) {
    const selector = browser.isAndroid
      ? SELECTORS.ANDROID.ALERT_TITLE
      : SELECTORS.IOS.ALERT;
    $(selector).waitForExist(11000, !isShown);
  }

  public static pressButton(selector) {
    const buttonSelector = browser.isAndroid
      ? SELECTORS.ANDROID.ALERT_BUTTON.replace(
          /{BUTTON_TEXT}/,
          selector.toUpperCase()
        )
      : `~${selector}`;
    $(buttonSelector).click();
  }

  /**
   * Get the alert text
   *
   * @return {string}
   */
  public static text() {
    // return driver.getAlertText();
    if (browser.isIOS) {
      return browser.getAlertText();
    }

    return `${$(SELECTORS.ANDROID.ALERT_TITLE).getText()}\n${$(
      SELECTORS.ANDROID.ALERT_MESSAGE
    ).getText()}`;
  }
}

export default NativeAlert;
