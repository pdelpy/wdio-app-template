import { expect } from "chai";
import { Given, When, Then } from '@cucumber/cucumber';
import TabBar from "../screens/components/TabBarComponent";
import FormScreen from "../screens/FormScreen";

const getSwitchState = (isActive: string): boolean => {
  return isActive === "on" ? true : false;
};

Given("i am on the form screen", async () => {
  await TabBar.waitForTabBarShown(true);
  await TabBar.openForms();
  await FormScreen.waitForIsShown(true);
});

Given("the switch init with {string} state", async (isActive: string) => {
  expect(await FormScreen.isSwitchActive()).to.equals(getSwitchState(isActive));
});

When("i type the text {string}", async (text: string) => {
  await (await FormScreen.input).setValue(text);
  if (browser.isKeyboardShown()) {
    browser.hideKeyboard();
  }
});

When("i click on the switch", async () => {
  await (await FormScreen.switch).click();
});

Then("the text result has the value {string}", async(text: string) => {
  expect(await (await FormScreen.inputTextResult).getText()).to.equals(text);
});

Then("the switch is {string}", async (isActive: string) => {
  expect(await FormScreen.isSwitchActive()).to.equals(getSwitchState(isActive));
});
