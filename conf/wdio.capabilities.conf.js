const { join } = require("path");

const local = {
  android: [
    {
      platformName: "Android",
      maxInstances: 1,
      avd: "Pixel_8.1_API_27",
      "appium:deviceName": "Pixel_8.1",
      "appium:platformVersion": "8.1",
      "appium:orientation": "PORTRAIT",
      "appium:automationName": "UiAutomator2",
      "appium:app": join(
        process.cwd(),
        "./apps/Android-NativeDemoApp-0.2.1.apk"
      ),
      "appium:noReset": false,
      "appium:newCommandTimeout": 240
    }
  ],
  ios: [
    {
      platformName: "iOS",
      maxInstances: 1,
      "appium:deviceName": "iPhone X",
      "appium:platformVersion": "12.0",
      "appium:orientation": "PORTRAIT",
      "appium:automationName": "XCUITest",
      "appium:app": join(
        process.cwd(),
        "./apps/iOS-Simulator-NativeDemoApp-0.2.1.app.zip"
      ),
      "appium:noReset": true,
      "appium:newCommandTimeout": 240
    }
  ]
};

// TO-DO faire le branchement a des device dans browserstack

// options de log visuel et de network
const logBs = {
  "browserstack.debug": true,
  "browserstack.networkLogs": false
};

const browserstack = {
  android: [
    {
      ...logBs,
      build: "Android",
      name: "Samsung Galaxy Note 10 Plus",
      device: "Samsung Galaxy Note 10 Plus",
      app: "AndroidApp"
    }
  ],
  ios: [
    {
      ...logBs,
      build: "Ios",
      name: "iPhone 11 Pro Max",
      device: "iPhone 11 Pro Max",
      app: "IosApp"
    }
  ]
};

const getLocalCapabilities = platform => {
  const platformLower = platform.toLowerCase();
  return local.hasOwnProperty(platformLower)
    ? local[platformLower]
    : local.android;
};

const getBrowserStackCapabilities = (platform, list) => {
  const platformLower = platform.toLowerCase();
  const capaList = list.split(",");
  if (browserstack.hasOwnProperty(platformLower)) {
    return browserstack[platformLower].filter(item => {
      return capaList.includes(item.name);
    });
  } else {
    return browserstack.android[0];
  }
};

exports.getLocalCapabilities = getLocalCapabilities;
exports.getBrowserStackCapabilities = getBrowserStackCapabilities;
