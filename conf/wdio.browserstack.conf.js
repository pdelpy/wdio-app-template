const mainConfig = require("./wdio.conf").config;
const { getBrowserStackCapabilities } = require("./wdio.capabilities.conf");

const platform = process.env.PLATFORM ? process.env.PLATFORM : "android";

// les différentes variables d'environnement sont : ANDROID_DEVICES, IOS_DEVICES
const list = process.env[`${platform.toUpperCase()}_DEVICES`]
  ? process.env[`${platform.toUpperCase()}_DEVICES`]
  : "Samsung Galaxy Note 10 Plus";

const capabilities = getBrowserStackCapabilities(platform, list);

const browserstackConfig = {
    ...mainConfig,
    capabilities,
    services: ['browserstack'],
    user: process.env.BROWSERSTACK_USER,
    key: process.env.BROWSERSTACK_KEY,
    browserstackLocal: true
};

exports.config = browserstackConfig;