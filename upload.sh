# upload local app into browserstack
curl -u "${BROWSERSTACK_USER}:${BROWSERSTACK_KEY}" \
-X POST "https://api-cloud.browserstack.com/app-automate/upload" \
-F "file=@$(pwd)/apps/Android-NativeDemoApp-0.2.1.apk" \
-F "data={\"custom_id\": \"AndroidApp\"}"